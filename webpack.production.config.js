const path = require('path')
const webpack = require('webpack')

module.exports = () => ({
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    extensions: ['.js', '.json', '.jsx']
  },
  output: {
    path:       path.join(__dirname, 'dist'),
    filename:   '[name].js',
  },
  module: {
    rules: [
      {
        test: [/\.jsx?$/, /\.js?$/],
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: ["@babel/react"],
          plugins: [ ["@babel/plugin-proposal-decorators", {legacy: true}], "@babel/plugin-proposal-class-properties", "react-css-modules",  "jsx-control-statements"]
        }
      },
      {
        test: [/\.css$/, /\.pcss$/,],
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: true,
              localIdentName: '[path]___[name]__[local]___[hash:base64:5]',
            },
          },
          { loader: 'postcss-loader' }
        ],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        query: {
          name: '[name].[ext]?[hash]'
        }
      }

    ]
  }
})
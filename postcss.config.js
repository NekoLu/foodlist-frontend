const cssImport = require('postcss-import');
const createResolver = require('postcss-import-webpack-resolver');

module.exports = {
  plugins: [
    cssImport({
      resolve: createResolver({
        // include where to look for modules
        modules: ['src', 'node_modules']
      })
    }),
    require('postcss-url'),
    require('postcss-preset-env')({
      browsers: 'last 2 versions',
      stage: 0,
    }),
    require('postcss-color-short'),
    require('postcss-focus'),
    require('precss'),
    require('postcss-size'),
    require('postcss-extend'),
  ],
};
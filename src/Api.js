function getUrlString(params, keys = [], isArray = false) {
  const p = Object.keys(params).map(key => {
    let val = params[key]

    if ('[object Object]' === Object.prototype.toString.call(val) || Array.isArray(val)) {
      if (Array.isArray(params)) {
        keys.push('')
      } else {
        keys.push(key)
      }
      return getUrlString(val, keys, Array.isArray(val))
    } else {
      let tKey = key

      if (keys.length > 0) {
        const tKeys = isArray ? keys : [...keys, key]
        tKey = tKeys.reduce((str, k) => {
          return '' === str ? k : `${str}[${k}]`
        }, '')
      }
      if (isArray) {
        return `${ tKey }[]=${ val }`
      } else {
        return `${ tKey }=${ val }`
      }

    }
  }).join('&')

  keys.pop()
  return p
}

function request(url, method = 'get', data = {}) {
  return new Promise((resolve, reject) => {
    url = api_url + url
    console.log(url)
    let options = {}
    url += '?' + encodeURI(getUrlString(data))
    if (method !== 'get') {
      options.method = method
      options.body = JSON.stringify(data)
    }
    if (method !== 'get') {

    }
    console.log(url)
    fetch(url, options).then((r) => {
      console.log('FETCHED')
      r.json().then(resp => {
        console.log(resp)
        if (resp.errors.length) {
          reject(resp)
        } else {
          resolve(resp)
        }
      })
    }).catch((err) => {
      console.error(err)
      reject(err)
    })
  })
}

export function getItems() {
  console.log('GET ITEMS')
  return new Promise((resolve, reject) => {
    request('/items').then((data) => {
      console.log(data)
      resolve(data.result.items)
    }).catch(reject)
  })
}

export function addItem(name, comment, count) {
  console.log('ADD ITEM')
  return new Promise((resolve, reject) => {
    request('/items', 'post', {item: {name: name, comment: comment, count: count}}).then((data) => {
      console.log(data)
      resolve(data.result.items)
    }).catch(reject)
  })
}

export function setBought(id) {
  console.log('BOUGHT ITEM')
  return new Promise((resolve, reject) => {
    request(`/items/${id}`, 'put', {item: {bought: true}}).then((data) => {
      console.log(data)
      resolve(data.result)
    }).catch(reject)
  })
}

export function setCount(id, count) {
  console.log('ITEM SET COUNT')
  return new Promise((resolve, reject) => {
    request(`/items/${id}`, 'put', {item: {count: count}}).then((data) => {
      console.log(data)
      resolve(data.result)
    }).catch(reject)
  })
}
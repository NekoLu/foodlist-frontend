import Baobab, { monkey } from 'baobab'

const defaultState = {
  items: [],
  addForm: false
}

export default new Baobab(defaultState)
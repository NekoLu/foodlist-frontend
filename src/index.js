import React from 'react'
import ReactDOM from 'react-dom'

import App from 'components/App'

import 'normalize.css'
import './styles/global.css'

import baobab from 'baobab'

window.baobab = baobab

ReactDOM.render(<App />, document.getElementById("main"))
import {getItems} from './Api'

export function loadAndSetItems(tree) {
  getItems().then((items) => {
    tree.select(['items']).set(items)
  })
}
import React, {Component} from 'react'
import {branch} from 'baobab-react/higher-order'


import './styles.css'
import Input from '../Input'
import Textarea from '../Textarea'
import Button from '../Button'
import {addItem, getItems} from '../../Api'
import {loadAndSetItems} from '../../actions'

@branch({isOpen: ['addForm']})
export default class AddForm extends Component {
  state = {
    name: '',
    comment: '',
    count: 1
  }

  onNameChange(e) {
    this.setState({name: e.target.value})
  }

  onCommentChange(e) {
    this.setState({comment: e.target.value})
  }

  onCountChange(e) {
    this.setState({count: e.target.value})
  }

  onClose(e) {
    this.props.dispatch(tree=>{
      tree.select(['addForm']).set(false)
    })
  }

  onSubmit(e) {
    addItem(this.state.name, this.state.comment, this.state.count).then(()=>{
      this.props.dispatch(tree=>{
        tree.select(['addForm']).set(false)
        loadAndSetItems(tree)
        this.setState({name: '', comment: '', count: 1})
      })
    })
  }

  render() {
    return <div styleName="wrapper" style={{display: this.props.isOpen? 'flex' : 'none'}}>
      <div styleName="modal">
        <span styleName="header">Добавить в список</span>
        <Input type={'text'} styleName="name" value={this.state.name} onChange={this.onNameChange.bind(this)} placeholder={'Продукт'} />
        <Textarea styleName="comment" value={this.state.comment} onChange={this.onCommentChange.bind(this)} placeholder={'Комментарий'} />
        <Input type={'number'} styleName="count" value={this.state.count} onChange={this.onCountChange.bind(this)} placeholder={'Количество'} />
        <div style={{marginTop: '16px'}}>
          <Button style={{float: 'left'}} value={'Закрыть'} onClick={this.onClose.bind(this)} />
          <Button style={{float: 'right'}} value={'Добавить'} onClick={this.onSubmit.bind(this)} />
        </div>
      </div>
    </div>
  }
}
import React, {Component} from 'react'
import {hot} from 'react-hot-loader'
import {root} from 'baobab-react/higher-order'

import tree from 'store.js'

import './styles.css'

import Navbar from 'components/Navbar'
import List from 'components/List'
import AddForm from 'components/AddForm'

@root(tree)
@hot(module)
export default class App extends Component {
  render() {
    return <div styleName="app">
        <Navbar/>

        <List/>

        <AddForm/>
      </div>
  }
}
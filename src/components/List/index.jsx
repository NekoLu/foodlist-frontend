import React, {Component} from 'react'
import {branch} from 'baobab-react/higher-order'

import {getItems} from 'Api.js'
import Item from 'components/Item'

import './styles.css'
import {loadAndSetItems} from '../../actions'

@branch({items: ['items']})
export default class List extends Component {
  componentDidMount() {
    this.props.dispatch(loadAndSetItems)
  }

  render() {
    return <div styleName="list">
      {this.props.items.map(item=><Item key={`item_${item.id}`} item={item} />)}
    </div>
  }
}
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import MaterialIcon from 'material-icons-react';

import './styles.css'
import {branch} from 'baobab-react/higher-order'
import {setBought, setCount} from '../../Api'
import {loadAndSetItems} from '../../actions'

@branch({})
export default class Item extends Component {
  state = {
    count: 1
  }

  onBoughtClick(e) {
    setBought(this.props.item.id).then(()=>{
      this.props.dispatch(loadAndSetItems)
    })
  }

  componentWillReceiveProps(nextProps, nextState) {
    console.log('RP!!!')
    if (nextProps.item.count !== this.state.count) {
      this.setState({count: nextProps.item.count})
    }
  }

  componentDidMount() {
    this.setState({count: this.props.item.count})
  }

  onCountChange(e) {
    if (e.target.value>0) {
      this.setState({count: e.target.value})
      setCount(this.props.item.id, e.target.value).then(()=>this.props.dispatch(loadAndSetItems))
    }
  }

  render() {
    let item = this.props.item
    return <div styleName="item">
      <div styleName="left">
        <span styleName="name">{item.name}</span>
        {item.comment? <span styleName="comment">{item.comment}</span> : ''}
      </div>
      <div styleName="right">
        <input type={'number'} value={this.state.count} styleName="count" onChange={this.onCountChange.bind(this)} />
        <div styleName="bought" onClick={this.onBoughtClick.bind(this)}>
          <MaterialIcon icon={'shopping_cart'} />
        </div>
      </div>
    </div>
  }

  static propTypes = {
    item: PropTypes.object.isRequired,
  }
}
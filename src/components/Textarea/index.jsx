import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';

import './styles.css'

export default class Textarea extends PureComponent {
  state = {
    isActive: false
  }

  onBlur(e) {
    let newState = {isActive: false}
    this.setState(newState)
  }

  onPlaceholderClick() {
    this.input.focus()
  }

  render() {
    return <div styleName="input-wrapper">
      <span onClick={this.onPlaceholderClick.bind(this)} styleName={this.state.isActive || this.props.value.length? "placeholder-active" : "placeholder"}>
        {this.props.placeholder}
        </span>
      <textarea
        ref={input=>this.input=input}
        onFocus={e=>this.setState({isActive: true})}
        onBlur={this.onBlur.bind(this)}
        styleName={this.state.isActive? "input-active" : "input"}
        value={this.props.value}
        onChange={this.props.onChange} />
    </div>
  }

  static propTypes = {
    placeholder: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.any.isRequired,
    type: PropTypes.string,
  }
}
import React, {PureComponent} from 'react'

import MaterialIcon from 'material-icons-react'

import './styles.css'
import {branch} from 'baobab-react/higher-order'
import {loadAndSetItems} from '../../actions'

@branch({})
export default class Navbar extends PureComponent {
  state = {
    title: 'Список покупок'
  }

  onAddClick(e) {
    this.props.dispatch(tree => tree.select(['addForm']).set(true))
  }

  onRefreshClick(e) {
    this.props.dispatch(loadAndSetItems)
  }

  render() {
    return <div styleName="navbar">
      <span styleName="title">{this.state.title}</span>
      <div styleName="right">
        <span styleName="icon" onClick={this.onRefreshClick.bind(this)}>
          <MaterialIcon icon={'refresh'} size={26}/>
        </span>
        <span styleName="icon" onClick={this.onAddClick.bind(this)}>
          <MaterialIcon icon={'add'} size={26}/>
        </span>
      </div>
    </div>
  }
}
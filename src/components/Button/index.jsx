import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';

import './styles.css'

export default class Button extends PureComponent {
  render() {
    return <button style={this.props.style} styleName="button" onClick={this.props.onClick}>{this.props.value}</button>
  }

  static propTypes = {
    onClick: PropTypes.func.isRequired,
    value: PropTypes.any.isRequired,
    style: PropTypes.object,
  }
}
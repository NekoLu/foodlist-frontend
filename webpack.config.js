const path = require('path')
const webpack = require('webpack')

module.exports = () => ({
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    extensions: ['.js', '.json', '.jsx']
  },
  devtool: 'cheap-module-source-map ',
  output: {
    path:       path.join(__dirname, 'dist'),
    filename:   '[name].js',
    publicPath: 'http://localhost:9000/',
    crossOriginLoading: 'use-credentials',
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000,
    hotOnly: true,
    headers: {
      "Access-Control-Allow-Origin": "http://localhost:3001",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization",
      "Access-Control-Allow-Credentials": 'true',
    },
  },
  module: {
    rules: [
      {
        test: [/\.jsx?$/, /\.js?$/],
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: ["@babel/react"],
          plugins: [ ["@babel/plugin-proposal-decorators", {legacy: true}], "@babel/plugin-proposal-class-properties", "react-css-modules", "react-hot-loader/babel", "jsx-control-statements"]
        }
      },
      {
        test: [/\.css$/, /\.pcss$/,],
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: true,
              localIdentName: '[path]___[name]__[local]___[hash:base64:5]',
            },
          },
          { loader: 'postcss-loader' }
        ],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        query: {
          name: '[name].[ext]?[hash]'
        }
      }

    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
})
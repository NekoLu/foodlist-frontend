const path = require('path')
const express = require('express')
const app = express()
const port = process.env.PORT || 3000;

// app.get('/', function (req, res) {
//   res.redirect('/app/')
// });

app.get('/app/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'prod_index.html'))
})

app.get('/service-worker.js', (req, res) => {
  res.sendFile(path.join(__dirname, 'service-worker.js'))
})

app.use('/dist', express.static('dist'));
app.use('/public', express.static('public'));

app.listen(port, function () {
  console.log('Example app listening on port 3001!');
});